#UDF Functions---------------------------------------------------------------------------------------------------
#udf wrapper function to use when creating udfs for pyspark  

#CharToNum = udf(lambda z: paymentsvalue_fmt(z), IntegerType())
#spark.udf.register("CharToNum", CharToNum)  

def udf_wrapper(returntype):
 
    def udf_func(func):
 
        return udf(func, returnType=returntype)
 
    return udf_func

#paymentsvalue_fmt-----------------------------------------------------------
#Function: Character to Numeric Conversion of paymentsvalue fields according to rules

@udf_wrapper(IntegerType())
def paymentsvalue_fmt(x): 
    if x == ' ':
        return -1
    elif x == '0':
        return 0
    elif x == '1':
        return 1
    elif x == '2':   
        return 2
    elif x == '3':
        return 3
    elif x == '4':   
        return 4
    elif x == '5':   
        return 5
    elif x == '6':   
        return 6
    elif x == 'X':   
        return 10
    else:
        return -2
#----------------------------------------------------------------------------

#blankcolumns-----------------------------------------------------------
#Function: Adds blank columns of a specific type
@udf_wrapper(IntegerType())
def blankcolumns_int(): 
  return None

@udf_wrapper(t.StringType())
def blankcolumns_str(): 
  return None
#----------------------------------------------------------------------------

#date_difference-----------------------------------------------------------
#Function: Calculates and returns the months between two dates.

@udf_wrapper(IntegerType())
def date_difference(latest,earlier):
  date_delta = (latest.year*12-earlier.year*12)+(latest.month-earlier.month)
  return date_delta
#----------------------------------------------------------------------------
#Assigning Columns-----------------------------------------------------------
#Function: Used to assing the correct paymentstatusvalues to the months after observation.

@udf_wrapper(IntegerType())
def assign_outcome(time_delta,month,*args):
  if time_delta < 0:
    return -2
  else:
    if time_delta-month < 0:
      return -2
    if month < time_delta - 23:
      return -2
    else:
      return args[time_delta - month]
    
#----------------------------------------------------------------------------
#Calculating Vintages-----------------------------------------------------------
#Function: Used to calculate Vintages.

@udf_wrapper(IntegerType())
def calculate_vintage(month,*args):
  Vintage_3 = max(args[0],args[1],args[2],args[3])
  Vintage_6 = max(Vintage_3,args[4],args[5],args[6])
  Vintage_9 = max(Vintage_6,args[7],args[8],args[9])
  Vintage_12 = max(Vintage_9,args[10],args[11],args[12])
  Vintage_15 = max(Vintage_12,args[13],args[14],args[15])
  Vintage_18 = max(Vintage_15,args[16],args[17],args[18])
  Vintage_21 = max(Vintage_18,args[19],args[20],args[21])
  Vintage_24 = max(Vintage_21,args[22],args[23],args[24])
  
  if month == 3:
    return Vintage_3
  elif month == 6:
    return Vintage_6
  elif month == 9:
    return Vintage_9
  elif month == 12:
    return Vintage_12
  elif month == 15:
    return Vintage_15
  elif month == 18:
    return Vintage_18
  elif month == 21:
    return Vintage_21
  elif month == 24:
    return Vintage_24
  else:
    return -3
  
    
#----------------------------------------------------------------------------
#Calculating Missing Values -1 which are expected but missing-----------------------------------------------------------
#Function: Used to calculate missing values -1.

@udf_wrapper(IntegerType())
def calculate_missing1(month,*args):
  if month == 3:
    Number_of_missing_3 = sum(1 if args[i] == -1 else 0 for i in range(4))
    return Number_of_missing_3
  elif month == 6:
    Number_of_missing_6 = sum(1 if args[i] == -1 else 0 for i in range(7))
    return Number_of_missing_6
  elif month == 9:
    Number_of_missing_9 = sum(1 if args[i] == -1 else 0 for i in range(10))
    return Number_of_missing_9
  elif month == 12:
    Number_of_missing_12 = sum(1 if args[i] == -1 else 0 for i in range(13))
    return Number_of_missing_12
  elif month == 15:
    Number_of_missing_15 = sum(1 if args[i] == -1 else 0 for i in range(16))
    return Number_of_missing_15
  elif month == 18:
    Number_of_missing_18 = sum(1 if args[i] == -1 else 0 for i in range(19))
    return Number_of_missing_18
  elif month == 21:
    Number_of_missing_21 = sum(1 if args[i] == -1 else 0 for i in range(22))
    return Number_of_missing_21
  elif month == 24:
    Number_of_missing_24 = sum(1 if args[i] == -1 else 0 for i in range(25))
    return Number_of_missing_24
  else:
    return -3
#--------------------------------------------------------------------------------------------------------------------------------------------------------  
#Calculating Missing Values -1 which are expected but missing-----------------------------------------------------------
#Function: Used to calculate missing values -1.

@udf_wrapper(IntegerType())
def calculate_missing2(month,*args):
  if month == 3:
    Number_of_missing_3 = sum(1 if args[i] == -2 else 0 for i in range(4))
    return Number_of_missing_3
  elif month == 6:
    Number_of_missing_6 = sum(1 if args[i] == -2 else 0 for i in range(7))
    return Number_of_missing_6
  elif month == 9:
    Number_of_missing_9 = sum(1 if args[i] == -2 else 0 for i in range(10))
    return Number_of_missing_9
  elif month == 12:
    Number_of_missing_12 = sum(1 if args[i] == -2 else 0 for i in range(13))
    return Number_of_missing_12
  elif month == 15:
    Number_of_missing_15 = sum(1 if args[i] == -2 else 0 for i in range(16))
    return Number_of_missing_15
  elif month == 18:
    Number_of_missing_18 = sum(1 if args[i] == -2 else 0 for i in range(19))
    return Number_of_missing_18
  elif month == 21:
    Number_of_missing_21 = sum(1 if args[i] == -2 else 0 for i in range(22))
    return Number_of_missing_21
  elif month == 24:
    Number_of_missing_24 = sum(1 if args[i] == -2 else 0 for i in range(25))
    return Number_of_missing_24
  else:
    return -3
#--------------------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------    
#--------------------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------    


#Data Frame Functions--------------------------------------------------------------------------------------------------------------------------------------------------------
#FUNCTION: Convert Payment Values from character to numeric----------------------------------------------------------------     
def numericRHIS(self):
  
  num_to_char={' ':'-1','X': '10'}

  columns = self.columns
  result = pd.Series(columns).str.contains(pat = 'paymentstatusvalue')
  old_columns = list(pd.Series(columns)[result])

  new_columns = []
  for old_column in old_columns:
    new_columns.append(old_column+"n")

  expression = []
  for o,n in zip(old_columns,new_columns):
    expression.append("cast("+o+" as int) "+o)

  expression2 = []
  for orig_columns in self.columns:
    if orig_columns not in old_columns:
      expression2.append(orig_columns)

  expression = list(np.concatenate((expression2,expression)))  


  self = self.replace(num_to_char,old_columns)
  self = self.selectExpr(expression)

  return self

DataFrame.numericRHIS = numericRHIS


#FUNCTION: Map Paymentstatusvalue to Month after observation date----------------------------------------------------------------     
def outcome_after_obs(self,prefix,number):

  #Getting all paymentstatusvalues required for the mapping of paymentstatus to Month
  columns = self.columns
  result = pd.Series(columns).str.contains(pat = 'paymentstatusvalue')
  arguments = list(pd.Series(columns)[result])
  
  
  cols = [self[j] for j in arguments]
  
  #Assigning paymentstatusvalues to the correct months after obs.
  
  for pmt_stat_nr in range(number):
    new_column = prefix+str(pmt_stat_nr)
    self = self.withColumn(new_column, assign_outcome('Date_Difference',lit(pmt_stat_nr),*cols))  
  
  
  return self

DataFrame.outcome_after_obs = outcome_after_obs



#STEP: Getting maximum arrears accross the same month----------------------------------------------------------------     

def Aggregate_over_Account(self,gby_list):

  columns = self.columns
  result = pd.Series(columns).str.contains(pat = 'Month')
  columns_to_max = list(pd.Series(columns)[result])

  newcols = []
  for column in columns_to_max:
    newcols.append([f.max(column).alias(column)])

  newcols.append([f.max('Open_time').alias('Open_time')])

  self =  self.\
  groupBy(*[gby_col for gby_col in gby_list]).\
  agg(*[item for sublist in newcols for item in sublist])
  
  
  return self

DataFrame.Aggregate_over_Account = Aggregate_over_Account

#---------------------------------------------------------------------------------------------------------------------
#FUNCTION: Getting values to be used for vintages----------------------------------------------------------------     
def vintages(self,vintages):

  #Getting all Months in the DataFrame
  columns = self.columns
  result = pd.Series(columns).str.contains(pat = 'Month')
  arguments = list(pd.Series(columns)[result])
  
  
  cols = [self[j] for j in arguments]
  
  #Returning Vintages from 3 to 24.
  
  for month in range(0,vintages+1,3):
    if month > 0:
      new_column = 'Vintage_'+str(month)
      self = self.withColumn(new_column, calculate_vintage(lit(month),*cols))
      new_column = 'Missing_1_'+str(month)
      self = self.withColumn(new_column, calculate_missing1(lit(month),*cols))
      new_column = 'Missing_2_'+str(month)
      self = self.withColumn(new_column, calculate_missing2(lit(month),*cols))
  
  
  return self

DataFrame.vintages = vintages


#---------------------------------------------------------------------------------------------------------------------
#FUNCTION: Convert Attribute Values from character to numeric----------------------------------------------------------------     
def Attribute_Char_Num(self):
  num_to_char={'U':'-1','X': '10','D': '10','-N': '10','P': '10'}

  char_columns = [f.name for f in self.schema.fields if isinstance(f.dataType, t.StringType)  and f.name not in ('aas_pin','enquiryaccounttypecode')]
  result = pd.Series(char_columns).str.contains(pat = 'pr')
  CCR_columns = list(pd.Series(char_columns)[result])

  new_columns = []
  for old_column in CCR_columns:
    new_columns.append(old_column+"n")

  expression = []
  for o,n in zip(CCR_columns,new_columns):
    expression.append("cast("+o+" as int) "+o)

  expression2 = []
  for orig_columns in self.columns:
    if orig_columns not in CCR_columns:
      expression2.append(orig_columns)

  expression = list(np.concatenate((expression2,expression)))  


  self = self.replace(num_to_char,CCR_columns)
  self = self.selectExpr(expression)

  return self

DataFrame.Attribute_Char_Num = Attribute_Char_Num
#---------------------------------------------------------------------------------------------------------------------