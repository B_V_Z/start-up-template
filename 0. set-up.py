#Bernie Changed This
#For Hive Access
import pyspark
from pyspark import SparkConf
from pyspark.context import SparkContext
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql.types import IntegerType
from pyspark.sql.window import Window
from pyspark.sql.functions import lag, lead, first, last, array, lit, udf, col, unix_timestamp
import pyspark.sql.functions as f
from pyspark.sql.functions import when
import pyspark.sql.types as t
import datetime
import time

#Pandas and numpy
import numpy as np
import pandas as pd
import math



#This first block creates the spark connection to the HIVE tables.
#the ".config" pieces are optional, but will allow some management of the connection.

spark = SparkSession.builder.appName("Session_Git").config("spark.yarn.queue", "root.edf_amp").getOrCreate()
    